<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model {
    protected $table = 'schedule';

    protected $fillable = [
        'type_id', 'from', 'to', 'f_name', 'l_name', 'email'
    ];
}
