<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use App\Model\User;
use \Firebase\JWT\JWT;

class ApiAuthMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next) {
        $resArr = [
            'message' => 'Unauthorized'
        ];
        $resCode = 401;
        try {
            $token = $request->header('Authorization');
            $secret = env('APP_SECRET');
            $decode = JWT::decode($token, $secret, array('HS256'));

            if (!isset($decode->id)) {
                return response()->json($resArr, $resCode);
            }

            $user = User::select('id', 'name', 'email')->where('id', $decode->id)->first();

            if (!$user) {
                return response()->json($resArr, $resCode);
            }
            $request->merge([
                'user_id' => $user->id,
                'user_name' => $user->name,
                'user_email' => $user->email,
                'user_token' => $token
            ]);
            return $next($request);
        } catch (\Exception $e) {
            $resArr['cause'] = $e->getMessage();
            return response()->json($resArr, $resCode);
        }
    }
}
