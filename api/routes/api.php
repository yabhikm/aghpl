<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('/ping', function () {
    return response()->json([
        'message' => 'ping successfully',
        'release_note' => 'project init'
    ], 200);
});

// Authenticated Routes
Route::group(['middleware' => 'api.auth'], function () {

    Route::get('/auth', ['as' => 'user_auth', 'uses' => 'UserController@auth']);

    // Event Types
    Route::group(['prefix' => 'event-types'], function () {
        Route::get('/', ['as' => 'get_event_types', 'uses' => 'EventTypeController@get']);
        Route::post('/', ['as' => 'add_event_types', 'uses' => 'EventTypeController@add']);
        Route::patch('/{id}', ['as' => 'update_event_types', 'uses' => 'EventTypeController@update']);
    });
});

// Unauthenticated Routes
Route::get('/user/{id}/event-types', ['as' => 'get_user_event_types', 'uses' => 'UserController@getUserEventTypes']);
Route::get(
    '/user/{id}/availability',
    ['as' => 'get_user_availability', 'uses' => 'UserController@getUserAvailability']
);

Route::post('/schedule', ['as' => 'add_schedule', 'uses' => 'UserController@addSchedule']);